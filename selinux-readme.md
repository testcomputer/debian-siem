# **SELinux Utilities and Libraries**

Security-Enhanced Linux (SELinux) is an advanced Mandatory Access Control (MAC) system integrated directly into the Linux Kernel. It provides enhanced access control granularity, surpassing the conventional Linux Discretionary Access Controls (DAC). This repository offers the source code for SELinux utilities and libraries, essential tools for setting up and managing an SELinux-based system.

---

## **Getting in Touch**

- **Bug Reports & Patches**: Send to the SELinux mailing list at [selinux@vger.kernel.org](mailto:selinux@vger.kernel.org).

- **Subscriptions**: To receive updates, draft an email with the body "subscribe selinux" and send to [majordomo@vger.kernel.org](mailto:majordomo@vger.kernel.org).

- **Mailing List Archive**: Access previous discussions and threads at [https://lore.kernel.org/selinux](https://lore.kernel.org/selinux).

---

## **Installation**

SELinux utilities and libraries are available in various Linux distributions, including:

- **Alpine Linux**: [View Package](https://pkgs.alpinelinux.org/package/edge/testing/x86/policycoreutils)
- **Arch Linux User Repository**: [View Package](https://aur.archlinux.org/packages/policycoreutils/)
- **Buildroot**: [View Source](https://git.buildroot.net/buildroot/tree/package/policycoreutils)
- **Debian & Ubuntu**: [View Package](https://packages.debian.org/sid/policycoreutils)
- **Gentoo**: [View Package](https://packages.gentoo.org/packages/sys-apps/policycoreutils)
- **RHEL & Fedora**: [View Source](https://src.fedoraproject.org/rpms/policycoreutils)
- **Yocto Project**: [View Source](http://git.yoctoproject.org/cgit/cgit.cgi/meta-selinux/tree/recipes-security/selinux)

For a comprehensive list, visit [https://repology.org/project/policycoreutils/versions](https://repology.org/project/policycoreutils/versions).

---

## **Building and Testing**

### **Dependencies**

#### Fedora:
- C libraries and programs:
    ```bash
    dnf install audit-libs-devel bison bzip2-devel CUnit-devel diffutils flex gcc gettext glib2-devel make libcap-devel libcap-ng-devel pam-devel pcre2-devel xmlto
    ```
- Python and Ruby bindings:
    ```bash
    dnf install python3-devel python3-pip python3-setuptools python3-wheel ruby-devel swig
    ```

#### Debian:
- C libraries and programs:
    ```bash
    apt-get install --no-install-recommends --no-install-suggests bison flex gawk gcc gettext make libaudit-dev libbz2-dev libcap-dev libcap-ng-dev libcunit1-dev libglib2.0-dev libpcre2-dev pkgconf python3 systemd xmlto
    ```
- Python and Ruby bindings:
    ```bash
    apt-get install --no-install-recommends --no-install-suggests python3-dev python3-pip python3-setuptools python3-wheel ruby-dev swig
    ```

### **Compilation**:

Follow the steps below:

1. Compile and install under a private directory:
    ```bash
    make clean distclean
    make DESTDIR=~/obj install install-rubywrap install-pywrap
    ```

2. **Note for Debian Users**: Maintain the correct python directory structure by setting:
    ```bash
    PYTHON_SETUP_ARGS='--install-option "--install-layout=deb"'
    ```

3. Adjust paths and run tests:
    ```bash
    DESTDIR=~/obj ./scripts/env_use_destdir make test
    ```

**Caution**: Overwriting pre-existing libraries and binaries may destabilize your system. Always backup and test in a secure environment.

---

## **macOS Installation**

1. Install `libsepol` for policy analysis:
    ```bash
    cd libsepol
    make PREFIX=/usr/local install
    ```

2. **Prerequisite**: Install GNU coreutils:
    ```bash
    brew install coreutils
    ```

---

Thank you for choosing SELinux Utilities and Libraries. Remember to always refer to documentation and test in a safe environment before making any system-level changes. Happy coding!
