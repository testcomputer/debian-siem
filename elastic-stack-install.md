Elastic Stack Installation

Install the latest versions of Elasticsearch, Logstash, and Kibana.

Elasticsearch

bash

Copy code
# Debian/Ubuntu
sudo apt install openjdk-11-jdk elasticsearch

# RHEL/CentOS  
sudo yum install java-11-openjdk elasticsearch

Enable Elasticsearch on boot:

bash

Copy code
sudo systemctl enable elasticsearch.service

Logstash

bash

Copy code
# Debian/Ubuntu
sudo apt install logstash

# RHEL/CentOS
sudo yum install logstash

Sample Logstash config pipeline to parse Nginx logs:

Copy code
input {
  beats {
    port => 5044 
  }  
}

filter {
  grok {
    match => { "message" => "%{COMBINEDAPACHELOG}"}
  }
  date {
    match => [ "timestamp" , "dd/MMM/yyyy:H:m:s Z" ]
  }
}

output {
  elasticsearch {
    hosts => ["elasticsearch.example.com:9200"]
    index => "nginx-logs-%{+YYYY.MM.dd}" 
  }
}

Kibana

bash

Copy code
# Debian/Ubuntu 
sudo apt install kibana

# RHEL/CentOS
sudo yum install kibana

Securing Elastic Stack

Protect Elasticsearch API with Nginx auth:

nginx

Copy code
location / {
  proxy_pass http://elasticsearch:9200;
  proxy_set_header  Authorization "Basic encoded-credentials";
}

Require TLS for Elasticsearch:

yaml

Copy code
xpack.security.enabled: true
xpack.security.transport.ssl.enabled: true
xpack.security.transport.ssl.verification_mode: certificate
xpack.security.transport.ssl.keystore.path: elastic-certificates.p12
xpack.security.transport.ssl.truststore.path: elastic-certificates.p12

Enable Elasticsearch audit logging:

Copy code
xpack.security.audit.enabled: true
xpack.security.audit.logfile: /var/log/elasticsearch/audit.log

Monitoring and Alerting

Create Kibana dashboard visualizing Audit log data. Set up Watcher alerts for failed login attempts:

json

Copy code
{
  "trigger": {
    "schedule": { "interval": "60m" }  
  },
  "input": {
    "search": {
      "request": {
        "index": "audit-*",
        "body": {
          "query": {
            "bool": {
              "must": [
                { "match": { "event.outcome": "failure" }},
                { "match": { "event.action": "authentication_failed" }}  
              ]
            }
          }
        }
      }
    }
  },
  "condition": {
    "compare": {
      "ctx.payload.hits.total": { "gt": 0 }
    }
  },
  "actions": {
    "email_admin": { 
      "email": {
        "to": "admins@example.com",
        "subject": "Failed login attempts detected",
        "body": "Investigate unauthorized access attempts"
      }
    }
  }
}

Security Enhancements

Install Wazuh for host monitoring and intrusion detection. Integrate with Elasticsearch:

Copy code
<wazuh_managers>
  <manager>
    <host>wazuh.example.com</host>
    <port>1514</port>
  </manager>
</wazuh_managers>

<wazuh_logging>
 <log_format>json</log_format> 
</wazuh_logging>

Forward security events to TheHive for case management:

Copy code
output {
  http {
    hosts => ["http://thehive.example.com:9000"]
    # Send alerts as cases to TheHive
    # ...
  }  
}

Let me know if you need any other specific examples or use cases for securing the Elastic Stack!
