If you want to see only the active services managed by systemd using systemctl, you can use the following command:

bash

systemctl list-units --type=service --state=active

Here's a breakdown:

    list-units: Lists the units that systemd is managing.
    --type=service: Filters the list to only show services.
    --state=active: Filters the list to show only active services.

This will give you a list of all active services and their current state (e.g., running).
