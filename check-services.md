If you want to disable these services on a Debian-based system (or any system using systemd), they are typically provided by the samba package.

Here's how you can disable and stop them:

    Stop the services:

    bash

sudo systemctl stop smbd nmbd

Disable the services so they don't start on boot:

bash

sudo systemctl disable smbd nmbd

If you no longer need Samba at all, you can remove the package:

bash

    sudo apt-get purge samba

After making these changes, you can run your nmap scan again to verify that the ports are no longer open.
