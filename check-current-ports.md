Using netstat:

bash

netstat -tuln

Using ss:

bash

ss -tuln

Both commands will show you a list of listening ports along with the associated addresses and protocols.


To check for listening ports on your system, you can use the netstat or ss command. If you don't have them installed, you can get them with:

bash

sudo apt-get install net-tools     # for netstat

or

bash

sudo apt-get install iproute2      # for ss (usually already installed by default
