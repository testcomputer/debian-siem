Here's how you can list all services that are enabled to start on boot:

bash

sudo systemctl list-unit-files --type=service | grep enabled

This command does the following:

    list-unit-files: Lists all unit files (services, sockets, mounts, etc.).
    --type=service: Filters the list to show only services.
    grep enabled: Filters the output to show only services that are enabled.

The resulting list will show you all services that are set to start automatically after a reboot.
